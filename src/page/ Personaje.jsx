import { useEffect, useState } from 'react';

import { BannerDetallePersonaje } from '../components/Personajes/BannerDetallePersonaje';
import { ListaEpisodio } from '../components/Episodio/ListaEpisodio';
import axios from 'axios';
import { useParams } from 'react-router-dom';

export function Personaje() {
    let { personajeId } = useParams();
    let [personaje, setPersonaje] = useState(null);
    let [episodios, setEpisodios] = useState(null);

    useEffect(() => {
        axios
            .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
            .then((answer) => {
                setPersonaje(answer.data);
            });
    }, []);

    useEffect(() => {
        if (personaje) {
            let peticionesEpisodios = personaje.episode.map((episodio) => {
                return axios.get(episodio);
            });

            Promise.all(peticionesEpisodios).then((answer) => {
                setEpisodios(answer);
            });
        }
    }, [personaje]);

    return (
        <div className='p-5'>
            {personaje ? (
                <div>
                    <BannerDetallePersonaje {...personaje} />

                    <h2 className='py-4'>Episodios</h2>
                    {episodios ? (
                        <ListaEpisodio episodios={episodios} />
                    ) : (
                        <div>Cargando episodios...</div>
                    )}
                </div>
            ) : (
                <div>Cargando...</div>
            )}
        </div>
    );
}


