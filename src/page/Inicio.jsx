import { Banner } from "../components/banner";
import { useState } from "react";
import { Search } from "../components/Search/Search";
import { ListPersonajes } from "../components/Personajes/ListPersonajes";


export function Home() {
    let [search, setSearch] = useState("");
    return (
        <div>
            <Banner />
            <div className="container">
                <h1 className="py-5">Personajes Destacados</h1>
            </div>
            <Search valor={search} onSearch={setSearch} />
            <ListPersonajes Search={search} />

        </div>
    );
}
