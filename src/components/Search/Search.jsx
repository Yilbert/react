export function Search({ valor, onSearch }) {
    return (
        <div className="d-flex justify-content-end py-5">

            <div className="form-outline">
                <input type="search" id="form1" placeholder="Search" className="form-control"
                    value={valor}
                    onChange={(evento) => onSearch(evento.target.value)} />

            </div>
        </div>


    );
}
