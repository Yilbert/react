function getstylestatus(status) {
    let color = "#08f54f";

    if (status === 'unknown') {
        color = '#1e14af';
    }

    if (status === 'Dead') {
        color = '#b80624';
    }

    const estiloCirculo = {

        color: color,

    };
    return estiloCirculo;
}
export function BannerDetallePersonaje({
    id,
    name,
    status,
    species,
    location,
    image,
    origin,
}) {
    return (
        <div className="id">
            <div className="col-6">

                <div
                    className="card text-white bg-dark mb-4"
                    style={{ maxWidth: "7000px" }}
                >
                    <div className="row g-0">
                        <div className="col-md-4">
                            <img src={image}
                                style={{ height: " 100% ", objectFit: "cover" }}
                                className="img-fluid rounded-start" alt={name} />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title mb-0">{name}</h5>
                                <p className="status">
                                    <span style={getstylestatus(status)}>
                                        {status} - {species}
                                    </span>
                                </p>
                                <p className="mb-0">Location</p>
                                <p>{location?.name}</p>
                                <p className="card-text">Origin</p>
                                <p className="card-text">
                                    <small className="">{origin?.name}</small>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div >
    );
}
