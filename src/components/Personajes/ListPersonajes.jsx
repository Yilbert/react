import { PersonajeItems } from "./PersonajeItems";
import { useState, useEffect } from "react";
import axios from "axios";


export function ListPersonajes({ Search }) {
    const [personajes, setPersonajes] = useState(null);
    let personajeSearch = personajes?.results;
    if (Search && personajes) {
        personajeSearch = personajes.results.filter((personaje) => {
            let nombrepersona = personaje.name.toLowerCase();
            let searchminus = Search.toLowerCase();
            return nombrepersona.includes(searchminus)
        });

    }









    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
            setPersonajes(respuesta.data);
        });
    }, []);


    return (
        <div className='row py-5'>
            {personajes
                ? personajeSearch.map((elemento) => {
                    return (
                        <PersonajeItems

                            key={elemento.id}
                            {...elemento}
                        />
                    );
                })
                : 'Cargando...'}
        </div>
    );
}