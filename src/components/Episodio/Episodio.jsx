export function Episodio({ name, air_date, episode }) {
  return (
    <div className="col-3">
      <div className="card border-primary mb-3">
        <div className="card-header">
          <h2>{name}</h2>
        </div>

        <p className="card-body text-primary">
          <h3>{episode}</h3>
        </p>
        <h3 className="card-body text-primary">{air_date}</h3>
      </div>
    </div>
  );
}
