import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { Home } from "./page/Inicio";
import { Personaje } from "./page/ Personaje";
import { Header } from "./nav-bar/Header";
import { Footer } from "./Footer/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personaje/:personajeId' element={<Personaje />} />

      </Routes>

      <Footer />
    </div>
  );
}

export default App;
