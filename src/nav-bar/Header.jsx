import { Link } from "react-router-dom";

export function Header() {
    return (
        <nav className="navbar navbar-expand-lg navbar-custom ">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">
                    <h1>RICK and Morty</h1>
                </Link>

            </div>
        </nav>
    );
}
